package com.key2act.LoginTestcases;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.key2act.BaseProperties.BaseClass;
import com.key2act.PageObjects.MasterpageFactory;

public class TC002_LoginTest extends BaseClass {
	

	MasterpageFactory lp;
	
	@BeforeTest
	public void browserSetup() throws IOException
	{
		BrowserInvoke();
		driver.get(prop.getProperty("url"));
		driver.manage().window().maximize();
	}
	@Test
	public void verifyLogindetails() throws InterruptedException
	{
		lp = new MasterpageFactory(driver);
		lp.Username.click();
		lp.Username.sendKeys(prop.getProperty("username"));
		lp.Password.sendKeys(prop.getProperty("password"));
		Thread.sleep(5000);		
		lp.Password.sendKeys(Keys.TAB);
//	    lp.WebDriverWait();
		lp.Login.click();
		Thread.sleep(30000);
		lp.ApplicationMenu.click();
		lp.Admin.click();
		lp.jcompany.click();
		lp.editc.click();
	}
	
	
	
	
	
}
