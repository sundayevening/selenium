package com.key2act.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MasterpageFactory {
	
	WebDriver driver;
	WebDriverWait wait;
	
	public MasterpageFactory(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, 20);
	}
	
	@FindBy(xpath="//input[@name='email']")
	public WebElement  Username;
	
	@FindBy(xpath="//*[@id='password']")
	public WebElement  Password;
	
	@FindBy(xpath="//*[text()='Sign In']")
	public WebElement  Login;
	
	@FindBy(xpath="//*[@class='jss201']")
	public WebElement signtout;
	/*@FindBy(xpath="	(//button[@type='button'])[3]")
	public WebElement Update;
	
	@FindBy(xpath="//*[@name='password']")
	public WebElement change;*/
	@FindBy(xpath="//*[text()='Create Company']")
	public WebElement company;
	
	@FindBy(xpath="//*[@id='name']")
	public  WebElement cname;
	
	@FindBy(xpath="(//i[text()='apps'])[1]")
	public WebElement ApplicationMenu;
	
	public void Wait()
	{
		wait.until(ExpectedConditions.visibilityOf(ApplicationMenu));
	}
    @FindBy(xpath="//a[text()='Admin']")

    public WebElement Admin;
    @FindBy(xpath="//span[text()='Manage Roles']")
    public WebElement userrole;
    @FindBy(xpath="//text()[.='New Role']/ancestor::button[1]")
    public WebElement editrole;

    @FindBy(xpath="//*[@id='name']")

    public WebElement Billing;

    @FindBy(xpath="//*[@id='email']")

    public WebElement billingemail;

    @FindBy(xpath="//*[@id='phone']")

    public WebElement billingphone;

    @FindBy(xpath="//*[@id='address1']")

    public WebElement billingadd;

    @FindBy(xpath="//*[@id='city']")

    public WebElement billingcity;

    @FindBy(xpath="//*[@id='state']")

    public WebElement billingstate;

    @FindBy(xpath="//*[@id='zip']")

    public WebElement billingzip;

    @FindBy(xpath="//span[text()='Save']")

    public WebElement billingsave;
    
  //  @FindBy(xpath=".//*[normalize-space(text()) and normalize-space(.)='Usman karim'])[1]/preceding::i[1]")
  //  public WebElement menu;
    @FindBy(linkText="Admin")
    public WebElement admin;
    @FindBy(xpath="//div[@id='root']/div/div/div[2]/div[3]/div/div/div/button/span/i")
    public WebElement jcompany;
    @FindBy(xpath="(//li[@role='menuitem'])[1]")
    public WebElement editc;
    
  
	
	
}
