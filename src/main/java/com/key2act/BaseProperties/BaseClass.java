package com.key2act.BaseProperties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	
	public WebDriver  driver;
	public Properties prop = new Properties();
	
	public void BrowserInvoke() throws IOException 
	{
		FileInputStream fis=new FileInputStream("C:\\Users\\Usman\\workspaceNew\\Key2Act\\src\\main\\java\\com\\key2act\\resources\\Key2act.properties");
		prop.load(fis); 	    
		//System.out.println(prop.getProperty("username"));
		if(prop.getProperty("browser").equalsIgnoreCase("chrome"))
		{
			//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/WebDrivers/chromedriver.exe");	
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver(); 
		    System.out.println("Chrome"); 
		    
		}
		else if(prop.getProperty("browser").equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver","D:\\geckodriver-v0.16.1-win64\\geckodriver.exe");
			driver = new FirefoxDriver();
			System.out.println("Firefox");
		}
		else{
			System.setProperty("webdriver.ie.driver","E:\\SeleniumWebDrivers\\IEDriverServer_x64_3.13.0\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
	}

}
